import express from 'express'

// App Imports
import serverInitialisation from './App/setup/serverInitialisation'
import startServer from "./App/setup/startServer"

// Create express server
const server = express();



// Setup server
serverInitialisation(server);


const PORT = process.env.PORT || 8000;
server.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
// Start server
// startServer(server);
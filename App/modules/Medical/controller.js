import MedicalServices from "./service";

const MedicalController = {
    registerMedical: (req, res) => {
        MedicalServices.registerMedical(req, result => {
            result.success
                ? res.status(201).send(result)
                : res.status(404).send(result)
        })
    },



 
    getAllVaccinsByDog: (req, res) => {
		MedicalServices.getAllVaccinsByDog(req, (result) => {
			result.success
				? res.status(200).send(result)
				: res.status(404).send(result);
		});
  },
    
        getAllSicksByDog: (req, res) => {
		MedicalServices.getAllSicksByDog(req, (result) => {
			result.success
				? res.status(200).send(result)
				: res.status(404).send(result);
		});
    },

    
    
    
    
    editMedical: (req, res) => {
      MedicalServices.editMedical(req, (result) => {
        result.success
          ? res.status(200).send(result)
        : res.status(404).send(result);
    });
  },
    
    
    deleteMedical: (req, res) => {
      MedicalServices.deleteMedical(req, (result) => {
        result.success
          ? res.status(200).send(result)
        : res.status(404).send(result);
    });
  },
    
    

};

export default MedicalController;

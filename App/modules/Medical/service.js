import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import config from "../../config/server.json";
import Queries from "./query";

const MedicalServices = {
registerMedical: (req, callback) => {
    Queries.registerMedical(
      req,
      response => {
        return callback({ success: true, message: response });
      },
      error => {
        return callback({ success: false, message: error });
      }
    )
  },

  
getAllVaccinsByDog: (req, callback) => {
		Queries.getAllVaccinsByDog(
			req,
			(response) => {
				return callback({
					success: true,
					message: "all vaccins of this dog",
					data: response,
				});
			},
			(error) => {
				return callback({ success: false, message: error });
			}
		);
	},


getAllSicksByDog: (req, callback) => {
		Queries.getAllSicksByDog(
			req,
			(response) => {
				return callback({
					success: true,
					message: "all vaccins of this dog",
					data: response,
				});
			},
			(error) => {
				return callback({ success: false, message: error });
			}
		);
	},





 editMedical: (req, callback) => {
            Queries.editMedical(
      req,
      (response) => {
        return callback({ success: true, message: response });
      },
      (error) => {
        return callback({ success: false, message: error });
      }
    );
      },

 

 deleteDog: (req, callback) => {
    Queries.deleteDog(
      req.params.id,
      (response) => {
        return callback({
          success: true,
          message: "dog deleted",
          data: response,
        });
      },
      (error) => {
        return callback({ success: false, message: error });
      }
    );
  },



};

export default MedicalServices;
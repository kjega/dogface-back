import express from "express";
import MedicalController from "./controller";

const router = express.Router();


router.post("/add-medical", MedicalController.registerMedical);
//edit medical based on id_medical
router.put("/edit-medical/:id_medical", MedicalController.editMedical);
router.put("/delete-medical/:id_medical", MedicalController.deleteMedical);

router.get("/vaccin/:id_dog", MedicalController.getAllVaccinsByDog);
router.get("/sick/:id_dog", MedicalController.getAllSicksByDog);



export default router;

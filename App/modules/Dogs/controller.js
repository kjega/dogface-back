import DogServices from "./service";
import { imageUpload } from '../../config/image-upload.json'
// var ImageKit = require("imagekit");

// var imagekit = new ImageKit({
//     publicKey : "public_sjCUqYX3imqW2vJVj51slPpS/AU=",
//     privateKey : "private_mhAA3aD1L2Vv0XXcRqwUlv6vhsc=",
//     urlEndpoint : "https://ik.imagekit.io/a4dfgmv1j6k"
// });

const DogController = {

	getAllDogsByUser: (req, res) => {
		DogServices.getAllDogsByUser(req, (result) => {
			result.success
				? res.status(200).send(result)
				: res.status(404).send(result);
		});
	},




  getDogById: (req, res) => {
    DogServices.getDogById(req, result => {
      result.success ? res.status(200).send(result) : res.status(404).send(result)
    })
  },


   registerDog: (req, res) => {
        DogServices.registerDog(req, result => {
            result.success
                ? res.status(201).send(result)
                : res.status(404).send(result)
        })
	},
   
  
  editDog: (req, res) => {
    DogServices.editDog(req, (result) => {
         result.success
        ? res.status(200).send(result)
        : res.status(404).send(result);
    });
  },
   
deleteDog: (req, res) => {
    DogServices.deleteDog(req, (result) => {
      result.success
        ? res.status(200).send(result)
        : res.status(404).send(result);
    });
  },


 
  addDogImage: (req, res) => {
      DogServices.addDogImage(req, (result) => {
      result.success
        ? res.status(200).send(result)
        : res.status(404).send(result);
    });
  },
   
   
 addDogImage1: (req, res) => {
    DogServices.addDogImage1(req, (result) => {
      result.success
        ? res.status(200).send(result)
        : res.status(404).send(result);
    });
  },
   







  deleteDogImage: (req, res) => {
     
    DogServices.deleteDogImage(req, (result) => {
      //Will be executed once the service is finished
      result.success
        ? res.status(200).send(result)
        : res.status(404).send(result);
    });
  },
   
   	getAllDogImages: (req, res) => {
		DogServices.getAllDogImages(req, (result) => {
			result.success
				? res.status(200).send(result)
				: res.status(404).send(result);
		});
	},

};

export default DogController;

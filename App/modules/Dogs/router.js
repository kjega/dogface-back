import express from "express";
import DogController from "./controller";

const router = express.Router();

const multer = require('multer');
const upload = multer();

//middleware -- filter to my request

router.get("/user/:id", DogController.getAllDogsByUser)
router.get("/:id", DogController.getDogById)
router.post("/register", DogController.registerDog);
router.put("/:id", DogController.editDog);
router.delete("/delete/:id", DogController.deleteDog);
router.post('/add-dog-image', upload.single('dogImage'), DogController.addDogImage);
                                          //name, maxCount
router.post('/add-dog-images', upload.array('dogImage',10), DogController.addDogImage1);
router.delete('/:id_image', DogController.deleteDogImage);
router.get("/images/:id_dog", DogController.getAllDogImages);





export default router;

//upload image to AWS/Imagekit -- > URL (which is to be saved in the database)
//multer - it is used for imgae/file upload (multipart(text, image) form-data)
//FOR AWS -- 
// multer, s3 (s3 bucket is an AWS service for file upload)

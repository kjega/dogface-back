import Queries from "./query";

const DogServices = {
	

	registerDog: (req, callback) => {
    Queries.registerDog(
      req,
      response => {
        return callback({ success: true, message: response });
      },
      error => {
        return callback({ success: false, message: error });
      }
    )
  },

deleteDog: (req, callback) => {
    Queries.deleteDog(
      req.params.id,
      (response) => {
        return callback({ success: true, message: response });
      },
      (error) => {
        return callback({ success: false, message: error });
      }
    );
  },








getDogById: (req, callback) => {
		Queries.getDogById(
			req,
			(response) => {
				return callback({
					success: true,
					message: "information de ce dog",
					data: response,
				});
			},
			(error) => {
				return callback({ success: false, message: error });
			}
		);
	},



  

getAllDogsByUser: (req, callback) => {
		Queries.getAllDogsByUser(
			req,
			(response) => {
				return callback({
					success: true,
					message: "all dogs of this user",
					data: response,
				});
			},
			(error) => {
				return callback({ success: false, message: error });
			}
		);
	},


editDog: (req, response) => {
    //     Queries.editDog(
    //   req,
    //       (response) => {
    //     console.log("response : ",response)
    //     return { success: true, message: "Data updated" };
    //   },
    //   (error) => {
    //     return callback({ success: false, message: error });
    //   }
    // );
  const dog = Queries.editDog(req);
  // console.log(dog);
 
     return response({ success: true, message: "dog details updated" });
    
    
  },

  

	addDogImage: (req, callback) => {
    Queries.addDogImage(
      req,
      response => {
        return callback({ success: true, message: response });
      },
      error => {
        return callback({ success: false, message: error });
      }
    )
  },

	addDogImage1: (req, callback) => {
    Queries.addDogImage1(
      req,
      response => {
        return callback({ success: true, message: response });
      },
      error => {
        return callback({ success: false, message: error });
      }
    )
  },





  deleteDogImage: (req, callback) => {
    Queries.deleteDogImage(
      req,
      response => {
        return callback({ success: true, message: response });
      },
      error => {
        return callback({ success: false, message: error });
      }
    )
  },

  
getAllDogImages: (req, callback) => {
		Queries.getAllDogImages(
			req,
			(response) => {
				return callback({
					success: true,
					message: "All images of dog",
					data: response,
				});
			},
			(error) => {
				return callback({ success: false, message: error });
			}
		);
	},



};

export default DogServices;

import UserQueries from "./query";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import config from "../../config/server.json";
import Queries from "./query";
import { response } from "express";

const UserServices = {

  getUserVet: (req, callback) => {
    UserQueries.getUserVet(req,
      response => {
        return callback({ success: true, message: response });
      },
      error => {
        return callback({ success: false, message: error });
      });
  },
 
getUser: (req, callback) => {
    UserQueries.getUser(req,
      response => {
        return callback({ success: true, message: response });
      },
      error => {
        return callback({ success: false, message: error });
      });
  },

  authenticate: async (body) => {
    let { email, password } = body;
    console.log(email, password);
    if (typeof email !== "string" || typeof password !== "string") {
      return {
        status: 400,
        payload: {
          success: false,
          message: "All fields are required and must be a string type",
        },
      };
    }
    const user = await UserQueries.getByEmail(email);
    console.log("Authentication user : ", user);
    if (user.length==0) {
      return {
        status: 403,
        payload: { success: false, message: "Username not found" },
      };
    }

    const passwordMatched = await bcrypt.compare(password,user[0].password);
    if (passwordMatched) {
      const token = jwt.sign(
        { id: user.id, role: user.user_role },
        config.secret
      );

      const { password, ...userWithoutPassword } = user;
      return {
        status: 200,
        payload: {
          success: true,
          message: "User correctly authenticated",
          data: { token: token, user: userWithoutPassword },
        },
      };
    }
    return {
      status: 403,
      payload: { success: false, message: "Username & password missmatch" },
    };
  },
////end authentication

 
  register: async (body) => {
    //let { username, email, password ,is_veterinay, address_cabinet, code_postal,ville,longitude, latitude} = body;
    let { username, email, password,telephone,is_veterinary, address, code_postal,ville,longitude, latitude} = body;

     console.log(is_veterinary);
    if (
      typeof username !== "string" ||
      typeof email !== "string" ||
      typeof password !== "string" 
   
    ) {
      return {
        status: 400,
        payload: {
          success: false,
          message: "All fields are required and must be a string type",
        },
      };
    }
  const user = await Queries.getByUserEmail(email);

    if (user) {
      return {
        status: 403,
        payload: { success: false, message: "Username existe" },
      };
    }

     return bcrypt
      .genSalt()
      .then((salt) => bcrypt.hash(password, salt))
      .then((hashedPassword) =>
      // UserQueries.register({ username,email,hashedPassword,is_veterinay,address_cabinet,code_postal,ville,longitude,latitude }))
        UserQueries.register({ username,email,hashedPassword,telephone,is_veterinary,address,code_postal,ville,longitude,latitude }))
   .then((user) => ({
        status: 201,
        payload: { success: true, message: "User successfully registered" },
      }))
      .catch((err) => ({
        status: 400,
        payload: { success: false, message: err },
      }));
  },










 delete: (req, callback) => {
    Queries.delete(
      req.params.id,
      (response) => {
        return callback({
          success: true,
          message: "user deleted",
          data: response,
        });
      },
      (error) => {
        return callback({ success: false, message: error });
      }
    );
  },

  editUser1: (req, callback) => {
      console.log(req.body.password);
   return bcrypt
      .genSalt()
  .then((salt) => bcrypt.hash(req.body.password, salt))
     .then((hashedPwd) => {
       req.body.password = hashedPwd;
         console.log("After hashed : ",req.body.password);
          Queries.editUserA(
      req,
      (response) => {
        return callback({ success: true, message: response });
      },
      (error) => {
        return callback({ success: false, message: error });
      }
    );
       })
 
  },

 
  
editUser: (req, response) => {
  const user = Queries.editUser(req);
     return response({ success: true, message: "USER  details updated" });
    },
 


};

export default UserServices;

import express from "express";
import UserController from "./controller";

const router = express.Router();


//Public routes
router.post("/authenticate", UserController.authenticate);
router.post("/register", UserController.register);

router.get("/:id", UserController.getUser);
router.get("/vet/:id", UserController.getUserVet);
router.delete("/:id", UserController.delete);
router.put("/:id", UserController.editUser);





export default router;



import VaccinServices from "./service";

const VaccinController = {
    registerMedical: (req, res) => {
        VaccinServices.registerMedical(req, result => {
            result.success
                ? res.status(201).send(result)
                : res.status(404).send(result)
        })
    },



 
    getAllVaccinsByDog: (req, res) => {
		VaccinServices.getAllVaccinsByDog(req, (result) => {
			result.success
				? res.status(200).send(result)
				: res.status(404).send(result);
		});
    },
    
    editMedical: (req, res) => {
      VaccinServices.editMedical(req, (result) => {
        result.success
          ? res.status(200).send(result)
        : res.status(404).send(result);
    });
  },

};

export default VaccinController;

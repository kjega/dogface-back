import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import config from "../../config/server.json";
import Queries from "./query";

const VaccinServices = {
registerMedical: (req, callback) => {
    Queries.registerMedical(
      req,
      response => {
        return callback({ success: true, message: response });
      },
      error => {
        return callback({ success: false, message: error });
      }
    )
  },

  
getAllVaccinsByDog: (req, callback) => {
		Queries.getAllVaccinsByDog(
			req,
			(response) => {
				return callback({
					success: true,
					message: "all vaccins of this dog",
					data: response,
				});
			},
			(error) => {
				return callback({ success: false, message: error });
			}
		);
	},

 editMedical: (req, callback) => {
  
          Queries.editMedical(
      req,
      (response) => {
        return callback({ success: true, message: response });
      },
      (error) => {
        return callback({ success: false, message: error });
      }
    );
      
      
 
  },

 

 delete: (req, callback) => {
    Queries.delete(
      req.params.id,
      (response) => {
        return callback({
          success: true,
          message: "user deleted",
          data: response,
        });
      },
      (error) => {
        return callback({ success: false, message: error });
      }
    );
  },



};

export default VaccinServices;

import express from "express";
import TrainningController from "./controller";

const router = express.Router();

router.get("/", TrainningController.getAll)
//router.get("/:id", TrainningController.getDogTrainning)
router.post("/", TrainningController.registerTraining)

router.put("/:id", TrainningController.editTraining);


export default router;
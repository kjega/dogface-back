import Queries from "./query";

const TrainningServices = {
	
getAll: (req, callback) => {
		Queries.getAll(
			req,
			(response) => {
				return callback({
					success: true,
					message: "dogs and user of the dogs retrieve",
					data: response,
				});
			},
			(error) => {
				return callback({ success: false, message: error });
			}
		);
	},


	registerTraining: (req, callback) => {
    Queries.registerTraining(
      req,
      response => {
        return callback({ success: true, message: response });
      },
      error => {
        return callback({ success: false, message: error });
      }
    )
	},
	
	
editTraining: (req, response) => {
    //     Queries.editDog(
    //   req,
    //       (response) => {
    //     console.log("response : ",response)
    //     return { success: true, message: "Data updated" };
    //   },
    //   (error) => {
    //     return callback({ success: false, message: error });
    //   }
    // );
  const dog = Queries.editTraining(req);
  // console.log(dog);
 
     return response({ success: true, message: "training details updated" });
    
    
  },




};

export default TrainningServices;

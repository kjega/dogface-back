import LessonQueries from "./query";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import config from "../../config/server.json";
import Queries from "./query";
import { response } from "express";

const LessonServices = {

  getAllStepsByIdLess: (req, callback) => {
    LessonQueries.getAllStepsByIdLess(req,
      response => {
        return callback({ success: true, message: response });
      },
      error => {
        return callback({ success: false, message: error });
      });
  },
getAllLessons: (req, callback) => {
    LessonQueries.getAllLessons(req,
      response => {
        return callback({ success: true, message: response });
      },
      error => {
        return callback({ success: false, message: error });
      });
  },
 
// getLessonByIdPack: (req, callback) => {
//     LessonQueries.getLessonByIdPack(req,
//       response => {
//         return callback({ success: true, message: response });
//       },
//       error => {
//         return callback({ success: false, message: error });
//       });
//   },

};

export default LessonServices;

import Queries from "./query";

const AppointmentServices = {


	
getAppointsByVet: (req, callback) => {
		Queries.getAppointsByVet(req,(response) => {
				return callback({
					success: true,
					message: "All appointments",
					data: response,
				});
			},
			(error) => {
				return callback({ success: false, message: error });
			}
		);
	},


    getAppointsByUser: (req, callback) => {
		Queries.getAppointsByUser(req,(response) => {
				return callback({
					success: true,
					message: "All appointments",
					data: response,
				});
			},
			(error) => {
				return callback({ success: false, message: error });
			}
		);
	},

    

};

export default AppointmentServices;
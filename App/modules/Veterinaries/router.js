import express from "express";
import VetController from "./controller";

const router = express.Router();

router.get("/", VetController.getAllVet);
router.get("/:id", VetController.getHoursByVet);
router.post("/take-appoint", VetController.takeAppointment);
router.post("/cancel-appoint", VetController.cancelAppointment);
//router.post("/display-time", VetController.displayTime);




export default router;
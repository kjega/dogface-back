import mysql, { Connection } from "mysql2";
import databaseConfig from "../config/database.json";

const connection = mysql.createConnection({
	host: databaseConfig.host,
	user: databaseConfig.user,
	password: databaseConfig.password,
	database: databaseConfig.database,
	port: databaseConfig.port,
	socketPath: databaseConfig.socketPath
});

connection.connect((err) => {
	if (err) {
		console.error("error connecting: " + err.stack);
		return;
	}

	console.log("connected as id " + connection.threadId);
});

export default connection;
